----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/15/2019 01:40:17 AM
-- Design Name: 
-- Module Name: main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.all;
USE ieee.numeric_std.ALL; 
use IEEE.STD_LOGIC_UNSIGNED.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
    Port ( Clk : in STD_LOGIC;
           Senzor : in STD_LOGIC;
           Rst : in STD_LOGIC;
           NextDist : in STD_LOGIC;
           an : out STD_LOGIC_VECTOR (3 downto 0);
           cat : out STD_LOGIC_VECTOR (6 downto 0));
end main;

architecture Behavioral of main is
signal ClkDiv,EnCnt,EnDistCalc,RstCounter,WR,RD,RstDist1: STD_LOGIC:='0';
signal TimeOut: STD_LOGIC_VECTOR(15 downto 0):=x"0000";
constant motorSpeed:INTEGER:=10; 
signal Dist,Dist1,Dist2: STD_LOGIC_VECTOR(7 downto 0):=x"00";
signal Senz:STD_LOGIC_VECTOR(3 downto 0):=x"0";

begin
div:entity WORK.clkDiv port map(Clk,EnCnt,ClkDiv);
counter:entity WORK.calcCounter port map(ClkDiv,EnCnt,RstCounter,TimeOut);
disp:entity WORK.displ7seg port map(Dist1(7 downto 4),Dist1(3 downto 0),Dist2(7 downto 4),Dist2(3 downto 0),Clk,an,cat);
Senz<="000"&Senzor;
RstCounter<=NextDist or Rst;
mem:entity WORK.mem port map(Clk,WR,RD,Dist,Rst,Dist1,Dist2);
comm:entity WORK.CommandUnit port map(Clk,Rst,Senzor,NextDist,WR,RD,EnCnt,EnDistCalc);

process(EnDistCalc,NextDist)
variable dis:INTEGER:=0;
begin
    if(NextDist='1')then Dist<=x"00";
    elsif(EnDistCalc='1')then
        dis:=motorSpeed*CONV_INTEGER(TimeOut);
        Dist<=CONV_STD_LOGIC_VECTOR(dis,8);
    end if;
end process;
end Behavioral;
