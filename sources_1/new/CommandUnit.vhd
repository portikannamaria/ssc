----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/15/2019 12:31:52 AM
-- Design Name: 
-- Module Name: CommandUnit - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CommandUnit is
  Port ( Clk:in STD_LOGIC;
        Rst: in STD_LOGIC;
        Senzor: in STD_LOGIC;
        NextD: in STD_LOGIC;
        WR:out STD_LOGIC;
        RD: out STD_LOGIC;
        EnCount: out STD_LOGIC;
        EnDistCalc:out STD_LOGIC );
end CommandUnit;

architecture Behavioral of CommandUnit is
type TIP_STARE is (idle, count, calcDist, mem, disp);     
signal Stare : TIP_STARE; 
 signal c: integer:=0;
begin
    process(Clk)
        begin
            if RISING_EDGE (Clk) then
                  if (Rst = '1') then
                       Stare <= idle;  
                       c<=0;         
                  else              
                       case Stare is                 
                            when idle =>if(NextD='1') then c<=1;end if; if(Senzor='1') then Stare<=count; end if;
                            when count => if(Senzor='0') then Stare<=calcDist;end if;
                            when calcDist => Stare<=mem;   
                            when mem => if(c=0) then Stare<=idle; else Stare<=disp; end if;  
                            when disp => Stare<=idle;  
                       end case;
                  end if;
          end if;
     end process; 
     process(Stare)
     begin
        case Stare is
            when idle => WR<='0';ENCount<='0';EnDistCalc<='0';RD<='0';
            when count => WR<='0';ENCount<='1';EnDistCalc<='0';RD<='0';
            when calcDist => WR<='0';ENCount<='0';EnDistCalc<='1';RD<='0';
            when mem => WR<='1'; ENCount<='0';EnDistCalc<='0';RD<='0';
            when disp => WR<='0';ENCount<='0';EnDistCalc<='0';RD<='1';
        end case;
     end process;
     

end Behavioral;
