library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity clkDiv is
    Port ( Clk : in STD_LOGIC;
           --Divisor : in STD_LOGIC_VECTOR ( 31 downto 0 ); -- t = 1/100Mhz * v, where v is the time we want
           ClkOut : out STD_LOGIC );
end clkDiv;

architecture Behavioral of clkDiv is

    signal ClkInner : STD_LOGIC := '0';

begin

    process (Clk)
    
        variable count : STD_LOGIC_VECTOR ( 31 downto 0 ) := x"00000000";
        
    begin
    
        if ( rising_edge ( Clk ) ) then
            count := count + 1; -- counting
            if (  count  = "00000010111110101111000010000000" ) then
                ClkInner <= NOT ClkInner;
                count := x"00000000";
                
            end if;
            
        end if;
        
    end process;
    
    ClkOut <= ClkInner;

end Behavioral;