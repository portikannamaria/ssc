library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;
use IEEE.STD_LOGIC_ARITH.all;


entity calcCounter is
    Port ( Clk:in STD_LOGIC;--divided clk
			En:in STD_LOGIC;
			Rst: in STD_LOGIC;
			TimeOut: out STD_LOGIC_VECTOR(15 downto 0));
end calcCounter;

architecture Behavioral of calcCounter is

begin

    process (Clk)
    
        variable timeSec : STD_LOGIC_VECTOR ( 15 downto 0 ) := x"0000";
        
    begin
        if ( rising_edge ( Clk ) ) then
			if (Rst = '1') then 
				timeSec := x"0000";
			else
				if (En = '1') then
					timeSec := timeSec + 1;
				end if;
            end if;
        end if;
    TimeOut <= timeSec;    
    end process;

end Behavioral;