library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity mem is
    Port ( Clk: in STD_LOGIC;
            WR: in STD_LOGIC;
            Dist1:in STD_LOGIC_VECTOR(7 downto 0);
			Rst: in STD_LOGIC;
			DistOut: out STD_LOGIC_VECTOR(7 downto 0));
end mem;

architecture Behavioral of mem is


begin

    process ( Clk)
      
    begin
    
        if ( rising_edge ( Clk ) ) then
            if(Rst='1') then
			     DistOut <= x"00";
		      elsif (WR='1') then
				DistOut <= dist1;
		      end if;
		end if;
        
    end process;

end Behavioral;