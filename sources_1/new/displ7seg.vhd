library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity displ7seg is
  Port (N1,N2,N3,N4:in std_logic_vector(3 downto 0);
  clk:in std_logic;
  an:out std_logic_vector(3 downto 0);
  cat:out std_logic_vector(6 downto 0));
end displ7seg;

architecture Behavioral of displ7seg is
signal sel:std_logic_vector(15 downto 0);
signal num:std_logic_vector(3 downto 0);
begin
  process(clk)
      begin
      if(rising_edge(clk)) then
      sel<=sel+1;
      end if;
  end process;
  process(sel(15 downto 14))
  begin
    case sel(15 downto 14) is
    when "00" => an<="0111";
    when "01" => an<="1011";
    when "10" => an<="1101";
    when "11" => an<="1110";
    when others => an<="0000";
    end case;
  end process;
  process(sel(15 downto 14),N4,N3,N2,N1)
    begin
      case sel(15 downto 14) is
      when "00" => num<=N4;
      when "01" => num<=N3;
      when "10" => num<=N2;
      when "11" => num<=N1;
      when others => num<="0000";
      end case;
    end process;
   process(num)
    begin
        case num is
        when "0000" => cat<="0000001";
        when "0001" => cat<="1001111";
        when "0010" => cat<="0010010";
        when "0011" => cat<="0000110";
        when "0100" => cat<="1001100";
        when "0101" => cat<="0100100";
        when "0110" => cat<="0100000";
        when "0111" => cat<="0001111";
        when "1000" => cat<="0000000";
        when "1001" => cat<="0000100";
        when "1010" => cat<="0001000";  -- 'A'
        when "1011" => cat<="1100000";  -- 'B'
        when "1100" => cat<="0110001";  -- 'C'
        when "1101" => cat<="1000010";  -- 'D'
        when "1110" => cat<="0110000";  -- 'E'
        when "1111" => cat<="0111000";  -- 'F'
        when others=>cat<="0000000";
      end case;
    end process;
end Behavioral;
