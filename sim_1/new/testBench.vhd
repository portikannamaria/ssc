----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/26/2019 05:48:00 PM
-- Design Name: 
-- Module Name: testBench - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity testBench is
--  Port ( );
end testBench;

architecture Behavioral of testBench is
constant CLK_PERIOD : TIME := 10 ns; 
signal Clk,Rst,Senzor,Start:STD_LOGIC;
signal an :STD_LOGIC_VECTOR (3 downto 0);
signal cat :STD_LOGIC_VECTOR (6 downto 0);
begin
gen_clk: process    
        begin       
            Clk <= '0';       
            wait for (CLK_PERIOD/2);       
            Clk <= '1';       
            wait for (CLK_PERIOD/2);    
    end process gen_clk; 
dut : entity WORK.main port map(Clk,Senzor,Rst,Start,an,cat);
testare: process
        begin 
            Rst <= '1';    
            wait for CLK_PERIOD;   
            Rst<='0'; 
            wait for CLK_PERIOD/2; 
            Senzor<= '1';    
            wait for 2000000000ns;  
            Senzor<= '0'; 
            wait for CLK_PERIOD;
            Rst <= '1';    
            wait for CLK_PERIOD;   
            Rst<='0';     
            wait for CLK_PERIOD/2;
            wait for 1000000ns;
            Senzor<= '1';    
            wait for 1000000000ns;     
            Senzor<= '0'; 
    end process;
end Behavioral;